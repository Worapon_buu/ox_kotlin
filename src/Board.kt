class Board (private var x: Player,private var o: Player) {

    private var table = Array(9) {'-'}
    private var winner: Player? = null
    private var turn = 0

    fun getTable () = this.table

    fun getWinner () = this.winner

    fun setBoard(index: Int, item: Char) {
        this.table[index] = item
    }

    fun getCurrentPlayer (): Player {
        if (this.turn % 2 == 0) return this.x
        return this.o
    }

    private fun changeTurn() { turn++ }

    fun isEnd():Boolean {
        if(checkWin()) {
            //print("1")
            setWinner()
            return true
        }
        if (checkDraw()){
            //print("2")
            return true
        }
        this.changeTurn()
        return false
    }

    private fun checkResult (index: Int, diff: Int): Boolean {
        if (this.table[index] == this.table[index + diff] && this.table[index + diff] == this.table[index + (diff * 2)]) return true
        return false
    }

    private fun checkRow( ):Boolean {
        if(this.checkResult(0,1)&&checkEmpty(0)) return true
        if(this.checkResult(3,1)&&checkEmpty(3)) return true
        if(this.checkResult(6,1)&&checkEmpty(6)) return true
        return false
    }

    private fun checkCol():Boolean {
        if(this.checkResult(0,3)&&checkEmpty(0)) return true
        if(this.checkResult(1,3)&&checkEmpty(1)) return true
        if(this.checkResult(2,3)&&checkEmpty(2)) return true
        return false
    }

    private fun checkDiagonal(): Boolean {
        if (this.checkResult(0, 4)&&checkEmpty(0)) return true;
        if (this.checkResult(2, 2)&&checkEmpty(2)) return true;
        return false;
    }

    private fun checkWin(): Boolean {
        if(checkRow()) return true
        if(checkCol()) return true
        if(checkDiagonal()) return true
        return false
    }

    private fun checkDraw(): Boolean {
        if( turn == 8) return true
        return false
    }

     fun checkEmpty(index: Int): Boolean{
        if(this.table[index] == '-') return false
        return true
    }

    fun setWinner() {
        this.winner = this.getCurrentPlayer()
    }




}