import java.lang.Exception
import java.util.*

class Game  {
    private var x: Player = Player('x')
    private var o: Player = Player('o')
    private var table = Board(x,o)
    var countTurn: Int = 0
    val kb = Scanner(System.`in`)

    private fun printTable(){
        println("0 1 2")
        var j=0
        val temp = table.getTable()
        for ((i, value) in temp.withIndex()) {
            print("$value ")
            if (i %3==2) {
                println(j)
                j++
            }
        }
    }

    private fun newGame() {
        table = Board(x,o)
    }

    private fun inputRowCol() {

        while(true) {
            try {
                println(table.getCurrentPlayer().getName()+" Turn")
                println("Please input row(0-2) col(0-2) : ")

                val row = kb.nextInt()
                val col = kb.nextInt()
                val index: Int = (row * 3) + col

                if(row< 0 || row>2){
                    println("value of row must between 0-2.")
                    continue
                }
                if(col< 0 || col>2){
                    println("value of col must between 0-2.")
                    continue
                }
                if(table.checkEmpty(index)){
                    println("This position was chosen please change.")
                    continue
                }

                table.setBoard(index, table.getCurrentPlayer().getName())
                countTurn =+1
                break
            }catch (e: Exception){
                println("Position Incorrect")
                if (kb.hasNext()) {
                    kb.nextLine()
                }

            }
        }
    }

    private fun play () {
        while (true) {
            printTable()
            inputRowCol()

            if(this.table.isEnd()) {
                printTable()
                printWinner()
                //print("3")
                break
            }
        }

    }

    fun start(){
        while(true) {
            newGame()
            play()
            if(!replay()) break
        }

    }

    private fun replay(): Boolean {
        while (true) {
            var ans: String
            print("Play Again  (y/n)")
            ans = kb.next()
            if (ans == "y" ) return true
            if (ans == "n") return false
         }
    }

    private fun printWinner() {
        if(this.table.getWinner() == null) {
            println("Draw")
        }else {
            println("Winner is "+this.table.getWinner()!!.getName()+".")
        }

    }



}




